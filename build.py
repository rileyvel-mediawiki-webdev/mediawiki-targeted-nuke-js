# concats all modules into main.js and output to stdout
import subprocess

all_mods = subprocess.getoutput("cat ./modules/*.js")
main_content = subprocess.getoutput("cat main.js")
print(main_content.replace("// INCLUDEMODS", all_mods))
