/**
 MediaWiki Targeted Nuke.js

 A simple gadget that allows for mass deletion from the search interface.

 ----

 @licence   0BSD
 @version   4
 @since     2020-08-22
 @author    rileyvel

 */

// wrap in big function to prevent scope leak
"use strict";
async function startMediaWikiTargetedNuke() {

    // INCLUDEMODS

    launch();

}

const targetedNukeInitialisationPromise = startMediaWikiTargetedNuke();