// App.js

/**
 * Checks whether this page should have the gadget activated.
 * @returns {boolean} true <=> page is eligible
 */
function checkPageEligibility() {
    return pageIsSearchResult() || pageIsWhatLinksHere();
}

/**
 * Checks whether current page is a search results page
 * @returns {boolean}
 */
let pageIsSearchResultCache;
function pageIsSearchResult() {

    if (!pageIsSearchResultCache) {
        const computedValue = mw.config.get("searchTerm") !== null;
        pageIsSearchResultCache = { value: computedValue };
    }

    return pageIsSearchResultCache.value;

}

/**
 * Checks whether current page is a what links here page
 * @returns {boolean}
 */
let pageIsWhatLinksHereCache;
function pageIsWhatLinksHere() {

    if (!pageIsWhatLinksHereCache) {
        const computedValue = document.querySelector("#mw-whatlinkshere-list") != null;
        pageIsWhatLinksHereCache = { value: computedValue };
    }

    return pageIsWhatLinksHereCache.value;

}

/**
 * Global handler for all kinds of errors.
 * At present, it simply notifies the user of the error.
 * @param {Error} error the error to handle
 */
function handleError(error) {
    // simply notify the user
    mw.notify(
        MSG_ERROR_UNCAUGHT.replace("$1", error.toString()),
        { type: "error" }
    );
}

/**
 * Simply starts up the whole operation
 */
function launch() {
    if (checkPageEligibility() === true) {
        insertBatchDeleteLink();
    }
}

