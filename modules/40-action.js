// Action.js

/**
 * Try to delete the specified page with MediaWiki API
 * @param title {string} the title to delete
 */
async function doDeletePage(title) {
    try {
        await apiEndpoint.postWithEditToken({
            "action": "delete",
            "format": "json",
            "title": title,
            "reason": constructDeleteReason()
        });
    } catch (error) {
        handleError(error);
    }
}

/**
 * Get a delete reason based on current page type
 * @returns {string}
 */
function constructDeleteReason() {

    const ident = "[assisted] [mediawiki-targeted-nuke.js]";

    if (pageIsWhatLinksHere()) {
        return `${ident} Targeted Nuke from [[${mw.config.get("wgPageName")}]]`;
    } else if (pageIsSearchResult()) {
        return `${ident} Targeted Nuke from search term \"${mw.config.get("searchTerm")}\"`;
    }

}

/**
 * Onclick handler for the "delete" link
 * @param {Event} event DOM event default arg
 */
async function deleteLinkOnClick(event) {

    if (!allResultTitles) {
        allResultTitles = getAllResultTitles();
    }

    // build the primary UI
    const primaryUIContainer = createPrimaryUI();

    // open UI dialog
    createDialog(primaryUIContainer);
    openDialog();

}

/**
 * A private function to ask the user "are you sure"
 * Throw up an error if the user says no, and silently return otherwise.
 * @private
 */
function _requestUserConfirmation() {
    const response = window.confirm(MSG_R_U_SURE);
    if (!response) {
        throw new Error("Cancelled by User");
    }
}

/**
 * Onclick handler for the "confirm" button
 * @param {Event} event DOM event default arg
 */
async function confirmButtonOnClick(event) {

    try {

        // prompt the user first
        _requestUserConfirmation();

        event.target.innerText = MSG_DELETING;
        const deletionPromises = [];

        for (const title of allResultTitles) {

            // find the target checkbox
            const targetID = getCheckboxIdByTitle(title);
            const thisCheckbox = document.getElementById(targetID);

            // delete the page if the checkbox remains checked
            if (thisCheckbox.checked === true) {
                deletionPromises.push(doDeletePage(title));
            }

        }

        await Promise.all(deletionPromises);

        event.target.innerText = MSG_DONE_REFRESHING;
        setTimeout(() => { location.reload() }, REFRESH_TIME_DELAY_MS);

    } catch (e) {
        handleError(e);
    }

}

