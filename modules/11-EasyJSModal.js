// Easy JS Modal Script
// ... to create modal dialogs very easily


/**
 * Inserts all dependencies of the dialog into DOM
 * Inspired by w3schools: https://www.w3schools.com/howto/howto_css_modals.asp
 */
function createDialog(contentElement) {

    const styleElement = createDialogStyleElement();
    const modalElement = createDialogModalElement(contentElement);

    // shorthand to inject into body
    const body = document.querySelector("body");
    const $ib = (element) => body.insertAdjacentElement("afterbegin", element);

    // do the injection
    $ib(styleElement);
    $ib(modalElement);

    // bind close button's onclick to closeDialog and cancel button
    document.querySelector(".vj-modal-close").addEventListener("click", closeDialog);

    // register another event listener on the modal itself.
    // close the dialog if the click lands on the backdrop, and not the complete modal itself.
    modalElement.addEventListener("click", (event) => {
        if (event.target === modalElement) {
            closeDialog();
        }
    });

    // done

}

function createDialogStyleElement() {

    const tempContainer = document.createElement("div");
    tempContainer.innerHTML = `
        <style type="text/css">
        /* The Modal (background) */
        .vj-modal {
          display: none; /* Hidden by default */
          position: fixed; /* Stay in place */
          z-index: 10; /* Sit on top */
          left: 0;
          top: 0;
          width: 100%; /* Full width */
          height: 100%; /* Full height */
          overflow: auto; /* Enable scroll if needed */
          background-color: rgb(0,0,0); /* Fallback color */
          background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        
        /* Modal Content/Box */
        .vj-modal-content {
          background-color: #fefefe;
          margin: 15% auto; /* 15% from the top and centered */
          padding: 20px;
          border: 1px solid #888;
          width: 80%; /* Could be more or less, depending on screen size */
          max-width: 650px;
        }
        
        /* The Close Button */
        .vj-modal-close {
          color: #aaa;
          float: right;
          font-size: 28px;
          font-weight: bold;
        }
        
        .vj-modal-close:hover,
        .vj-modal-close:focus {
          color: black;
          text-decoration: none;
          cursor: pointer;
        }
        
        </style>`;

    return tempContainer.firstElementChild;

}

function createDialogModalElement(content) {

    const modalElement = document.createElement("div");
    modalElement.classList.add("vj-modal");

    const dialogContent = document.createElement("div");
    dialogContent.classList.add("vj-modal-content");
    dialogContent.insertAdjacentElement("afterbegin", content);

    dialogContent.insertAdjacentHTML("afterbegin", '<span class="vj-modal-close">&times;</span>');
    modalElement.insertAdjacentElement("beforeend", dialogContent);
    return modalElement;

}

/**
 * Opens the dialog
 */
function openDialog() {
    findModalElement().style.display = "block";
}

/**
 * Closes the dialog (hiding from view)
 */
function closeDialog() {
    findModalElement().style.display = null;
}

/**
 * From the DOM, find the modal element for operation.
 * Requires that the elements be already injected into the DOM.
 * @returns {HTMLElement}
 */
function findModalElement() {
    return document.querySelector(".vj-modal");
}

