// Parser.js

/**
 * Returns all titles (MediaWiki style) of the pages turned up in the search result.
 * @returns {String[]} all the titles
 */
function getAllResultTitles() {

    let result;

    if (pageIsSearchResult()) {
        result = _getSearchResultLinks();
    } else if (pageIsWhatLinksHere()) {
        result = _getWhatLinksHereLinks();
    } else {
        throw new Error("You shouldn't reach this point!!!");
    }

    return result.map(_extractTitleFromLink);

}

/**
 * Gets all links from a "what links here" page
 * @returns {HTMLAnchorElement[]}
 * @private
 */
function _getWhatLinksHereLinks() {

    const result = [];

    const containerList = document.querySelector("#mw-whatlinkshere-list");
    const allListItems = containerList.querySelectorAll("li");

    for (const item of allListItems) {
        result.push(item.querySelector("a"));  // only the first one in each <li> counts
    }

    return result;

}

/**
 * A function to get all search result links
 * @returns {HTMLAnchorElement[]}
 * @private
 */
function _getSearchResultLinks() {

    const result = [];
    const allResultHeadings = document.querySelectorAll(".mw-search-result-heading");

    // register the first A element in each mw-search-result-heading
    for (const heading of allResultHeadings) {
        result.push(heading.querySelector("a"));
    }

    return result;

    // exp.test passed on 2020-06-03

}

/**
 * Extracts result title from one single link element
 * @param linkElement {HTMLAnchorElement} element
 * @returns {string} extracted
 * @private
 */
function _extractTitleFromLink(linkElement) {

    // try regex first
    const extractor = new RegExp(articlePathRegexpString, "i");
    const href = decodeURI(linkElement.href);
    const match = href.match(extractor);

    if (match) {
        return match[1];
    }

    // didn't work? try another way
    const urlObject = new URL(linkElement.href, location.toString());
    if (urlObject.searchParams.has("title")) {
        return urlObject.searchParams.get("title");
    }

    // Can't figure this out.
    throw new TypeError(`Cannot parse link to ${linkElement.href}`);

}