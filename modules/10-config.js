// Config.js

// App runtime "globals"
const apiEndpoint = new mw.Api();
const articlePathRegexpString = `^.*${mw.config.get("wgArticlePath").replace("$1", "(.+)")}$`;
const linkInsertionPoint = document.querySelector("#t-specialpages");
let allResultTitles = null;  // to be filled at run time

// Message constants
const MSG_ERROR_UNCAUGHT = "Targeted_Nuke Error: $1";
const MSG_R_U_SURE = "Are you sure you want to delete these pages?";
const HEADER_TEXT = "Batch Delete the Following Pages";
const PROMPT_TEXT = "Select pages to delete:";
const LINK_TEXT = "Delete Batch...";
const CONFIRM_BUTTON_TEXT = "Confirm";
const REFRESH_TIME_DELAY_MS = 2000;
const MSG_DELETING = "deleting...";
const MSG_DONE_REFRESHING = "Done. Refreshing...";

