// UI.js

/**
 * Simply generate and insert the "delete" link
 */
function insertBatchDeleteLink() {
    linkInsertionPoint.insertAdjacentElement("afterend", _generateBatchDeleteLink());
}

/**
 * Generates the "bulk delete" link
 * @returns {Node} link generated
 * @private
 */
function _generateBatchDeleteLink() {

    // simply shallow clone the wlh element
    const newElmt = linkInsertionPoint.cloneNode(false);

    // replace its content with a new A element, and add onClick behaviour
    newElmt.id = "t-targeted-nuke";
    newElmt.appendChild(document.createElement("a"));
    newElmt.querySelector("a").innerText = LINK_TEXT;
    newElmt.addEventListener("click", deleteLinkOnClick);

    // Done.
    return newElmt;

}

/**
 * Generates the primary UI elements given all parsed result titles
 * @returns {HTMLDivElement} the generated element
 */
function createPrimaryUI() {

    const result = document.createElement("div");

    // create the header and prompt
    _createAndAppendUIHeading().forEach(elmt => { result.insertAdjacentElement("beforeend", elmt) });

    // create container for the checkboxes
    const checkBoxesContainer = document.createElement("ul");
    checkBoxesContainer.id = "targeted-nuke-checkboxes";
    result.insertAdjacentElement("beforeend", checkBoxesContainer);

    // create all checkboxes...
    for (const title of allResultTitles) {

        const thisLiElmt = document.createElement("li");
        thisLiElmt.insertAdjacentElement("beforeend", _createCheckbox(title));
        checkBoxesContainer.insertAdjacentElement("beforeend", thisLiElmt);

    }

    // create confirm button
    const button = document.createElement("button");
    button.classList.add("mw-ui-button");
    button.classList.add("mw-ui-destructive");
    button.innerText = CONFIRM_BUTTON_TEXT;
    button.addEventListener("click", confirmButtonOnClick);
    result.insertAdjacentElement("beforeend", button);

    // done
    return result;

}

/**
 * Generates the fixed top part of the UI, and append them into the given element
 * @returns {HTMLElement[]} the generated stuff
 * @private
 */
function _createAndAppendUIHeading() {

    let result = [];

    const header = document.createElement("h2");
    header.textContent = HEADER_TEXT;
    result.push(header);

    const prompt = document.createElement("p");
    prompt.innerText = PROMPT_TEXT;
    result.push(prompt);

    return result;

}

/**
 * Calculates the checkbox CSS ID from a title
 * @param title {string} the title to calculate for
 * @returns {string} the ID
 */
function getCheckboxIdByTitle(title) {
    return `targeted-nuke-checkbox-for-${title}`;
}

/**
 * Generates a single checkbox element from a title
 * @param {string} title the title to generate a checkbox for
 * @returns {HTMLElement} the generated element
 */
function _createCheckbox(title) {

    const checkboxID = getCheckboxIdByTitle(title);

    // create the checkbox
    const thisCheckbox = document.createElement("input");
    thisCheckbox.type = "checkbox";
    thisCheckbox.checked = true;
    thisCheckbox.id = checkboxID;

    // create the label
    const thisLabel = document.createElement("label");
    thisLabel.insertAdjacentElement("afterbegin", thisCheckbox);
    thisLabel.insertAdjacentText("beforeend", title.replace("_", " "));
    return thisLabel;

}

